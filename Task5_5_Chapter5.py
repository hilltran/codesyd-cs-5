# My practise exercise
# smallest = None
# print('Before')
# for value in [1,-100,3,54353,5,46]:
#   if smallest is None:
#     smallest = value
#   elif smallest > value:
#     smallest = value
# print("After: The smallest number is " + str(smallest))


##Start Excercise_________________________________________
# Exercise 1: Write a program which repeatedly reads numbers until the user enters "done". Once "done" is entered, print out the total, count, and average of the numbers. If the user enters anything other than a number, detect their mistake using try and except and print an error message and skip to the next number.

# Enter a number: 4
# Enter a number: 5
# Enter a number: bad data
# Invalid input
# Enter a number: 7
# Enter a number: done
# 16 3 5.33333333333333

##Code Start______
# total = 0
# count = 0

# while True:
#   user_input = input("Enter a number ")
#   if user_input == 'done':
#     break
#   try:
#       number = float(user_input)
#   except:
#       print('Invalid input')
#   total = total + number
#   count = count + 1

# average = total/count
# print(total,count,average)
## End Code______

##Start Excercise_________________________________________
# Exercise 2: Write another program that prompts for a list of numbers as above and at the end prints out both the maximum and minimum of the numbers instead of the average.

#Code Start______
biggest = None
smallest = None
number = None

while True:
  user_input = input("Enter a number ")
  if user_input == 'done':break
  try:
    number = float(user_input)
  except:
    print('Invalid input')
  if biggest is None or number > biggest :
    biggest = number
  if smallest is None or number < smallest:
    smallest = number

print(f'Biggest Number is {biggest} and the Smallest Number is {smallest}')
##Code Start______
