# # Exercise 1: Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.
# Updated on 10-3-19
# ##Code Start______
# hours = float(input('Enter Hours: '))
# rate= float(input('Enter Rate: '))
# payment =0
# if hours <= 40:
#   payment= hours*rate
# else:
#   payment= str(40*rate) + ((hours - 40)*1.5*rate)
# print(f"Pay: {payment}")
#
# ## End Code______

# Exercise 2: Rewrite your pay program using try and except

##Code Start______version 2 with quit() after the except 10-3-19
# try:
#   hours = float(input('Enter Hours: '))
# except:
#   print('Error, please enter numeric input')
#   quit()
#
# try:
#   rate= float(input('Enter Rate: '))
# except:
#   print('Error, please enter numeric input')
#   quit() # quit the program rather then trackback error
# payment =0
# if hours <= 40:
#   payment= hours*rate
# else:
#   payment= (40*rate) + ((hours - 40)*1.5*rate)
# print("Pay: " + str(payment))
# ## End Code______
#
# # Exercise 3: Write a program to prompt for a score between 0.0 and 1.0. If the score is out of range, print an error message. If the score is between 0.0 and 1.0, print a grade using the following table:
#
# #  Score   Grade
# # >= 0.9     A
# # >= 0.8     B
# # >= 0.7     C
# # >= 0.6     D
# #  < 0.6     F
#
# ##Code Start______Version 1
# prompt = "Please enter score between 0.0 and 1.0\n"
# score = (input(prompt))
# if score =='perfect':
#   print("Bad Score")
# else:
#   score = float(score)
#   if score >= 0.9 and score <=1.0:
#     print("A")
#   elif score >= 0.8 and score <0.9:
#     print("B")
#   elif score >= 0.7 and score <0.8:
#     print("C")
#   elif score >= 0.6 and score <0.7:
#     print("D")
#   elif score < 0.5:
#     print("F")
#   else:
#     print("Bad Score")
# ## End Code______
#
#   # Enter score: 0.95
# # A
# # Enter score: perfect
# # Bad score
# # Enter score: 10.0
# # Bad score
# # Enter score: 0.75
# # C
# # Enter score: 0.5
# # F
#
# # my personal notes
# # if it was using try/except
# # try:
# #   prompt = "Please enter score between 0.0 and 1.0\n"
# #   score = float(input(prompt))
# # except:
# #   print('Error, please enter numeric input between 0.0 and 1.0\n')
# #   prompt = "Please enter score between 0.0 and 1.0\n"
# #   score = float(input(prompt))

# ##Code Start______Version 2 on 10-3-19
# score = None
# prompt = "Please enter score between 0.0 and 1.0\n"
# score = (input(prompt))
# while score != None:
#   try:
#     if score > 0 and score < 1:
#       score = float(score)
#   except:
#     print("Please enter score between 0.0 and 1.0")
#     break
# score = (input("Enter only score between 0.0 and 1.0 \n"))
# # try:
# # score > 0 and score < 1:
# score = float(score)
# # except:
# #   print("Please enter score between 0.0 and 1.0 next time. Goodbye")
# #   quit()
# if score >= 0.9 and score <=1.0:
#   print("A")
# elif score >= 0.8:
#   print("B")
# elif score >= 0.7:
#   print("C")
# elif score >= 0.6:
#   print("D")
# elif score < 0.5:
#   print("F")
# else:
#   print("Bad Score")
# ## End Code______

# ##Code Start______Version 3 on 10-3-19
score = None
prompt = "Please enter score between 0.0 and 1.0\n"
score = (input(prompt))
while score != None:
  try:
    if score > 0 and score < 1:
      score = float(score)
  except:
    break
score = (input("Enter only score between 0.0 and 1.0 \n"))
try:
  score = float(score)
except:
  print("Please enter score between 0.0 and 1.0 next time. Goodbye")
  quit()
if score >= 0.9 and score <=1.0:
  print("A")
elif score >= 0.8 and score <0.9:
  print("B")
elif score >= 0.7 and score <0.8:
  print("C")
elif score >= 0.6 and score <0.7:
  print("D")
elif score < 0.5 and score >=0: # the reason for not using 'elif score >= 0.8' instead of 'elif score >= 0.8 and score <0.9:' because if it a number larger or = to 1 or 0 or -minus number it would get a grade.
  print("F")
else:
  print("Bad Score")

# ## End Code______
