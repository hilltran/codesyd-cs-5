
##Start Excercise_________________________________________
##Code Start______
## End Code______  

# Task 6.3 - Chapter 6 Exercises 
# Exercise 1: Write a while loop that starts at the last character in the string and works its way backwards to the first character in the string, printing each letter on a separate line, except backwards.
##Code Start______
movie = 'Titanic'
letter = len(movie)
n = letter - 1
while n > -1:
  print(movie[n])
  n = n - 1
## End Code______ 

##Start Excercise_________________________________________
# Exercise 2: Given that fruit is a string, what does fruit[:] mean?
# Answer: It mean from the beginning to the end, ie the whole string.
# eg.
# fruit = 'banana'
# print(fruit[:])

##Start Excercise_________________________________________
# Exercise 3: Encapsulate this code in a function named count, and generalize it so that it accepts the string and the letter as arguments.
##Code Start______
def count(word):
  count = 0
  for letter in word:
    if letter == 'a':
      count = count +1
  print(count)
count('Testing this count ithadk kenaaaaaanikenas ieinve')
## End Code______ 

##Start Excercise_________________________________________
# **Exercise 4: There is a string method called count that is similar to the function in the previous exercise. Read the documentation of this method at:
# str.count(sub[, start[, end]])
# sentence = 'Mary had a little lamb'
# sentence.count('a')
##Code Start______
fruit = 'banana'
# print(fruit.count('an'))
print(fruit.count('a'))
## End Code______ 

##Start Excercise_________________________________________
# Exercise 5: Take the following Python code that stores a string:
str_value = 'X-DSPAM-Confidence:0.8475'
# Use find and string slicing to extract the portion of the string after the colon character and then use the float function to convert the extracted string into a floating point number.
##Code Start______
colon_index = str_value.find(":")
print(colon_index)

num = str_value[colon_index+1:]
float_num = float(num)
print(float_num)
## End Code______ 

##Start Excercise_________________________________________
# Exercise 6: Read the documentation of the string methods at https://docs.python.org/library/stdtypes.html#string-methods You might want to experiment with some of them to make sure you understand how they work. strip and replace are particularly useful.

# The documentation uses a syntax that might be confusing. For example, in find(sub[, start[, end]]), the brackets indicate optional arguments. So sub is required, but start is optional, and if you include start, then end is optional.
# str.find(sub[, start[, end]])
##Code Start______
str = "1bcdefioshgoihgs sijsiojs "
##### "1234567890123456789012345 "
print(str.find('a'))
print(str.find('b'))
#index
print(str.find('g'))
# 10
print(str.find('s',11))
# 15
print(str.find('s',15))
# 15
print(str.find('j',16))
# 19
print(str.find('s',11,14))
# -1
## End Code______ 